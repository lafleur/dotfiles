# ~/.vim/\* the way I use it

Most of the plugins I use come from the package installer on my systems (see
`$ pacman -Qs vim-plugins`). Just a few are not there, and are installed as
submodules of this git repo (see `$ git submodule status`). Files in _this_ git
repo are only tweaks for optional plugins. If you don't have the plugin
installed, they should fail silently. Now for the details :

`00-attic` is where I put scripts that I don't use anymore.

`after` : the folders in this folder get loaded after initialisation, which
means they can override settings. I use `after/plugin` to add my own shorcuts.

`autoload` : quite self-explanatory I think

`colors` : self-explanatory as well ; guess it would succeed to do anything else
than colors anyway.

`ftplugin` : would contain plugins that get loaded on specific filetypes editing.
See `:help ftplugins`. The focused filetype is based on the `.vim` file name.
See `:help filetype` for an overview, and `:help ft-<TABCOMPLETION>` for info on
each file type options, e.g. `:help ft-html-syntax`.

`pack` : contains packages, which can be made of several plugins each. See `:help
packages` for details. `pack/foo/start/foobar` would be autoloaded at startup.
Note that this is vim > 8 only. It's Vim8's own plugin system.

`plugin` : may contain plugins, made of a single `.vim` file each. Most plugins
lie in eg /usr/share/.vim, and let you define plugin tweaks here. See `:help
plugin`.

`swap` : I don't know really. `.swp` files are elsewhere (I'll update when I get
it back, promess).

`vimfiles` : overrides the `/usr/share/vim/vimfiles` folder. I used to use it
for the tags plugin, can't remember why.

