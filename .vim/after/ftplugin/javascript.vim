if exists("g:ale_enabled")
  let b:ale_linters = {'javascript': ['eslint']}
endif
setlocal shiftwidth=2
setlocal tabstop=2

