" Match git graph's blocks into fugitive's logic :
"
syn match gitGraphLine     /^[_\*|\/\\ ]\+\(\<\x\{7\}\>.*\)\?$/ keepend
"syn match gitGraphHead     /^[_\*|\/\\ ]\+\(\<\x\{4,40\}\> - ([^)]\+)\( ([^)]\+)\)\? \)\?/ contained containedin=gitGraphLine
"syn match gitGraphDate     /(\u\l\l \u\l\l \d\=\d \d\d:\d\d:\d\d \d\d\d\d)/ contained containedin=gitGraphHead nextgroup=gitGraphRefs skipwhite

" Match any of _ * | / \ <Space> any number of times
syn match gitGraphBefore    /^[_\*|\/\\ ]\+/ contained containedin=gitGraphLine nextgroup=gitGraphHash skipwhite

" Match from a beginning of line to the last -<Space>
"syn match gitGraphHash   /^[^-]\+- / contained containedin=gitGraphHead nextgroup=gitGraphDate skipwhite

" Match a word made of 7 hexadecimal characters :
syn match gitGraphHash   /\(\<\x\{7\}\>\)/ contained containedin=gitGraphLine nextgroup=gitGraphRefs skipwhite
"syn match gitGraphIdentity /<[^>]*>$/ contained containedin=gitGraphLine

" Match a parenthesis, any character but the closing paren any number of
" times, and a closing paren.
syn match gitGraphRefs     /\(([^)]*)\)/ contained containedin=gitGraphLine nextgroup=gitGraphComment skipwhite

syn match gitGraphComment     /\(\S\)\+$/ contained containedin=gitGraphLine skipwhite


hi def link gitGraphBefore    Comment
"hi def link gitGraphDate      gitDate
hi def link gitGraphHash      Identifier

"hi def link gitGraphRefs      gitReference
hi def link gitGraphRefs      Label

"hi def link gitGraphIdentity  gitIdentity

