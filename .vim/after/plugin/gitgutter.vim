" Replace ]c and [c shortcuts :
"nmap <Leader>hh :GitGutterNextHunk<CR>
nmap <Leader>hh <Plug>(GitGutterNextHunk)
nmap <Leader>hH <Plug>(GitGutterPrevHunk)

" You should already have <Leader>hs and <Leader>hu to (un)stage a hunk.
