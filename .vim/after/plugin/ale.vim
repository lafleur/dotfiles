" vim-ale settings

let g:ale_lint_on_text_changed = 'never'
"let g:ale_lint_on_insert_leave = 0

nmap <silent> <leader>k <Plug>(ale_previous_wrap)
nmap <silent> <leader>j <Plug>(ale_next_wrap)
nmap <silent> <leader>, <Plug>(ale_detail)
nmap <silent> <leader>l <Plug>(ale_hover)
nmap gd :ALEGoToDefinition<CR>
nmap gt :ALEGoToTypeDefinition<CR>

"let g:ale_set_signs = 0

" Autocompletion support
let g:ale_completion_autoimport = 1
" Lets ctrl-x ctrl-o open ale completion
set omnifunc=ale#completion#OmniFunc

" Open in quickfix instead of loclist :
"let g:ale_set_quickfix = 1

"let g:ale_warn_about_trailing_whitespace = 0
let g:ale_warn_about_trailing_blank_lines = 0

" Remap tab, shift-tab and enter to useful completion shortcuts on popups :
"inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
"inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
"inoremap <expr> <cr>    pumvisible() ? "\<C-y>" : "\<cr>"

