" Tagbar plugin options
"
let g:tagbar_left="1"
"let g:tagbar_autoclose="1"
let g:tagbar_autofocus="1"

" Toggle the tagbar, get there, and close on tag select.
"nmap <leader>t :call PushToLeft("TagbarOpenAutoClose")<CR>
"let tagbar_map_close='<leader>t'
nmap <leader>t :TagbarToggle<CR>

