scriptencoding utf-8
" Hide mode from last line :
set noshowmode
let g:lightline = {
  \   'colorscheme': 'ThemerVimLightline',
  \   'active': {
  \     'left': [ [ 'mode', 'paste' ], [ 'gitbranch', 'file', 'readonly' ], ],
  \     'right': [ [ 'linter_checking', 'linter_errors', 'linter_warnings', 'linter_infos', 'linter_ok', 'percent', 'lineinfo', 'filetype' ], ],
  \   },
  \   'inactive': {
  \   'left': [ [ 'gitbranch', 'file' ] ],
  \   'right': [],
  \   },
  \   'component': {
  \     'lineinfo': '%l:%v',
  \   },
  \   'component_function': {
  \     'file': 'LightlineFile',
  \     'gitbranch': 'LightlineGitBranch',
  \     'percent': 'LightlinePercent',
  \     'filetype': 'LightlineFileType',
  \   }
  \ }
let g:lightline.separator = {
  \   'left': '', 'right': ''
  \}
let g:lightline.subseparator = {
  \   'left': '', 'right': '' 
  \}
let g:lightline.component_expand = {
  \  'linter_checking': 'lightline#ale#checking',
  \  'linter_infos': 'lightline#ale#infos',
  \  'linter_warnings': 'lightline#ale#warnings',
  \  'linter_errors': 'lightline#ale#errors',
  \  'linter_ok': 'lightline#ale#ok',
  \ }
let g:lightline.component_type = {
  \     'linter_checking': 'right',
  \     'linter_infos': 'right',
  \     'linter_warnings': 'warning',
  \     'linter_errors': 'error',
  \     'linter_ok': 'right',
  \ }

let g:lightline#ale#indicator_checking = "\uf110"
let g:lightline#ale#indicator_infos = "\uf129"
let g:lightline#ale#indicator_warnings = "\uf071"
let g:lightline#ale#indicator_errors = "\uf05e"
let g:lightline#ale#indicator_ok = "\uf00c"

function! LightlinePercent()
  return winwidth(0) > 70 ? line('.') * 100 / line('$') . '%' : ''
endfunction

function! LightlineGitBranch()
  return winwidth(0) > 70 ? fugitive#head() : fugitive#head()[0:2]
endfunction

function! LightlineFile()
  " If this is a git object, append [git] to the filename :
  " We could give the branch and revision of this file, see that later.
  if match(expand('%'), '^fugitive') != -1
    let filename = expand('%:t') . ' [git]'
  else
    let filename = expand('%:t')
  endif
  let filename .= &modified ? ' +' : ''
  return filename
endfunction

function! LightlineFileType()
  return winwidth(0) > 70 ? (&filetype !=# '' ? &filetype : 'no ft') : ''
endfunction

function! LightlineFileEncoding()
  return winwidth(0) > 70 ? &fileencoding : ''
endfunction
