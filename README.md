# My dotfiles
Those are my dotfiles ! They are copyleft, so you don't even have to quote me
to use them.

## Installation
This is rather old-school. you might want to go somewhere sensible for you and
``` sh
git clone https://framagit.org/lafleur/dotfiles
cd dotfiles
```
And then pick some of the following :
```
ln -s $(pwd)/.bashrc ~/.bashrc
ln -s $(pwd)/.zshrc ~/.zshrc
ln -s $(pwd)/.shell ~/.shell
ln -s $(pwd)/.gitconfig ~/.gitconfig
ln -s $(pwd)/nvim ~/.config/nvim
ln -s $(pwd)/.vim ~/.vim
ln -s $(pwd)/.vimrc ~/.vimrc
ln -s $(pwd)/.Xresources ~/.Xresources
```
`.shell` contains some sub-scripts sourced by both bashrc and zshrc.
.vim and nvim both have some submodules to pull some useful plugins, like this :
``` sh
git submodule init [hit tab to get completion on the available submodules]
```
For instance, `.gitignore` relies on
.vim/pack/diffconflicts/start/diffconflicts . It also states my username as a
remote login, be sure to check it out before sourcing.

I use some other plugins that are provided by Arch or its AUR repository, namely :
  - vim-fugitive
  - vim-gitgutter
  - vim-indexed-search-git
  - vim-lightline
  - vim-auto-pairs
  - vim-tagbar
  - vim-mesonic
  - vim-gtk-recent-git
  - vim-vala-git
  - avim.git-git
  - python-pynvim (needed by vim-gtk-recent-git)

I use nvim nightly release, that provides a language server protocol implementation.
Those are the language servers that I have installed for it to use :
  - vim-language-server
  - pyright
  - typescript-language-server-bin
  - vscode-css-languageserver-bin
  - vscode-html-languageserver-bin
  - yaml-languageserver-bin
  - nodejs-vls for vue.js
  - vala-language-server
  - rls (for rust, provided using `rustup component add rls rustfmt`)

Oh and I use the awesome themer to give all those folks some harmonious
semantic colours.

I think that's it, enjoy !
