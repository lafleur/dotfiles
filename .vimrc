"sleep 5
" free to get inspired of, indeed


" Get the defaults that most users want.
"
"source /usr/share/vim/vim82/defaults.vim
runtime defaults.vim

" Set space as mapleader, only used for special plugins shortcuts (see plugins).
let mapleader = ' '

" Helper function to push a buffer left and make it not as wide.
function PushToLeft(subCommand)
    exe ':vertical' a:subCommand
    silent exe 'vertical resize' &columns / 3
endfunction

func CountWords()
    exe 'normal g\'
    let words = substitute(v:statusmsg, '^.*Word [^ ]* of ', '', '')
    let words = substitute(words, ';.*', '', '')
    return words
endfunc

" Helper function that should move to somewhere else I guess.
function! WordCount()
   let s:old_status = v:statusmsg
   let position = getpos('.')
   exe ":silent normal g\<c-g>"
   let stat = v:statusmsg
   let s:word_count = 0
   if stat !=# '--No lines in buffer--'
     let s:word_count = str2nr(split(v:statusmsg)[11])
     let v:statusmsg = s:old_status
   end
   call setpos('.', position)
   return s:word_count 
endfunction


" Basics
"
set encoding=utf-8

" have spell check - can be tiring if you parse code
"set spell

" Include matching uppercase words with lowercase search term
set ignorecase
" Include only uppercase words with uppercase search term
set smartcase
" Traverse line breaks with arrow keys
"set whichwrap=b,s,<,>,[,]

" Backup specs
"
set undolevels=1000
if has('vms')
    set nobackup    " do not keep a backup file, use versions instead
else
    set backup      " keep a backup file (restore to previous version)
    if has('persistent_undo')
        set undofile    " keep an undo file (undo changes after closing)
    endif
endif

" If terminal has [colour capabilities ?] or we are using GUI,
if &t_Co > 2 || has('gui_running')
    " switch on highlighting the last used search pattern
    set hlsearch
endif


" Indentation
"
" always set autoindenting on.
set autoindent
filetype plugin indent on
" show existing tab with this many spaces width :
"set tabstop=4
" when indenting with '>', use this number of spaces width :
set shiftwidth=2
" On pressing tab, insert this same number of spaces :
set expandtab
" Split on the right side by default :
set splitright
" Let lightline display itself on initial opening :
set laststatus=2

" Copy / Paste
"
" let Vim use the clipboard register as its own (unnamed) clipboard :
"set clipboard+=unnamed
set clipboard+=unnamedplus
"set guioptions+=a


" Shortcuts
"

" Fold all lines once (doesn't iterate to get paragraphs back) :
nmap <leader>Z :g/^/norm gww

" Let g] (list tags) become gT because it's logical and alt-) is so bad.
" We lose the previous tab shortcut, will stick to gt (next tab).
nmap gT g]

" Bring the taglist left menu, and then go over there. Manage the initial width.
"nmap <leader>l :call PushToLeft('TlistToggle')<CR>

" Find the word under the cursor in the files in local folder, and open
" quickfix window on the left handside.
nmap <leader>F :call PushToLeft('vimgrep /' . expand('<cword>') . "/j *\n:vert cw")<CR>
nmap <leader>Fr :call PushToLeft('vimgrep /' . expand('<cword>') . "/j **\n:vert cw")<CR>

" Jump to next/previous error or warning according to the linter :
"nmap <leader>n :cnext<CR>
"nmap <leader>b :cprevious<CR>

" Search for the definition of the current word according to cscope :
"nmap <leader>gd :cs find g <C-R>=expand("<cword>")<CR><CR>
" Search for the definition of the current word according to ALE linter :

" :ter brings up a terminal, but it goes topmost and takes half the buffersize
" Let T bring a new 1-row terminal below all buffers
"nmap <leader>T :botright terminal ++rows=12<CR>
nmap <leader>T :tabnew<CR>:terminal<CR>
" Esc key twice will exit insert mode in terminal :
tmap <Esc><Esc> <C-w>N

" Change local dir to that of the local file. See :help filename-modifiers .
nmap <leader>cd :lcd %:p:h<CR>
" Vim's auto indentation feature does not work properly with text copied from
" outside of Vim so set paste-toggle to F5 .
set pastetoggle=<F5>

" Let <Shift-u> redo because I never remember ctrl-r mapping.
nmap U <C-r>
" Let <Shift-t> jump to tag because ctr-alt-) is so bad on french keyboards.
nmap T <C-]>

" Toggle Netrw explorer (in current folder). See Netrw further.
"nmap <leader>N :let g:netrw_winsize = &columns / 5<CR> :Lexplore<CR>
nmap <leader>e :Explore<CR>

" If you restore selection after a visual mode yank, you lose yank message.
vnoremap Y y gv
" Paste and select the newly copied text :
"noremap p p`[V`]
"  move text and rehighlight -- vim tip_id=224 :
vnoremap > ><CR>gv
vnoremap < <<CR>gv


" Presentation
"
set background=dark
colorscheme ThemerVim
"colorscheme pablo
"colorscheme koehler
hi Search cterm=NONE ctermfg=black ctermbg=blue

set guifont=FiraCode\ Nerd\ Font\ 13

" Let the current line be signaled when we're in Insert mode :
autocmd InsertEnter,InsertLeave * set cul!

" Break lines at word (requires Wrap lines) :
set linebreak
" Wrap-broken line prefix - !!! followed by a '\ ' intentionally !
set showbreak=+++\ 

" Self-explanatory - but think about your eyes, honey ...
"set visualbell

" Let preview window open on the bottom (J)
augroup previewWindowPosition
   au!
   autocmd BufWinEnter * call PreviewWindowPosition()
augroup END
function! PreviewWindowPosition()
   if &previewwindow
      wincmd J
   endif
endfunction


"  Feature support
"
" Have Vala ctags support
"let tlist_vala_settings='c#;d:macro;t:typedef;n:namespace;c:class;'.
"  \ 'E:event;g:enum;s:struct;i:interface;'.
"  \ 'p:properties;m:method'
" Have vala ctags through anjuta-tags
autocmd BufRead,BufNewFile *.vala,*.vapi,*.gs let tagbar_ctags_bin='anjuta-tags'

" powerline_fonts don't work if set up in ~/.vim/after/...
let g:airline_powerline_fonts = 1

" Disable vim-lsp because here we use vim-ale :
let g:lsp_auto_enable = 0

" Enable ale completion where available. This setting must be set before ALE
" is loaded.
let g:ale_completion_enabled = 1
" Alternatively, use deoplete :
"let g:deoplete#enable_at_startup = 1

" Wrapping
"
" Wrap markdown files at 80 characters :
autocmd BufRead,BufNewFile *.md setlocal textwidth=80

" Wrap at 80 whatsoever :
"set textwidth=80

" Highlight cols over 80 in dark grey :
"augroup vimrc_autocmds
"    autocmd BufEnter * highlight OverLength ctermbg=darkgrey guibg=#592929
"    autocmd BufEnter * match OverLength /\%81v.*/
"augroup END

" Cursor shape : | when inserting, _ when replacing, block in command mode
let &t_SI = "\<Esc>[6 q"
let &t_SR = "\<Esc>[4 q"
let &t_EI = "\<Esc>[2 q"

" Buggy features
"
" cscope startup - I'm rather using ctags and tagfinder.
"if has("cscope")
"    set cscopetag
"    " check cscope for definition of a symbol before checking ctags: set to 1
"    " if you want the reverse search order.
"    set csto=0
"    " add any cscope database in current directory
"    if filereadable("cscope.out")
"        cs add cscope.out  
"    " else add the database pointed to by environment variable 
"    elseif $CSCOPE_DB != ""
"        cs add $CSCOPE_DB
"    endif
"    " show msg when any other cscope db added
"    set cscopeverbose  
"endif

" Hack to include IDF_PATH to ctags ; gets in the way when you work elsewhere !
"au FileType c set tags^=$IDF_PATH/tags
"set path^=$IDF_PATH/**/include

" Vundle config
"
"set nocompatible              " be iMproved, required
"filetype off                  " required
" set the runtime path to include Vundle and initialize :
"set rtp+=~/.vim/bundle/Vundle.vim
"call vundle#begin()
"Plugin 'arrufat/vala.vim'
"call vundle#end()            " required
"filetype plugin indent on    " required

" base16 theme blob
"let base16colorspace=256  " Access colors present in 256 colorspace
"colorscheme base16-ashes
"set termguicolors

