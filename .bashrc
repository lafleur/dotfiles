# ~/.bashrc: executed by bash(1) for non-login shells, see ~/.profile for login.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

if test -e ~/.shell/aliases.sh; then
    source ~/.shell/aliases.sh
fi
if test -e ~/.shell/rc.sh; then
    source ~/.shell/rc.sh
fi
if test -e ~/.shell/message.sh; then
    source ~/.shell/message.sh
fi

# Activate extended completion under Arch
compl=/usr/share/bash-completion/bash_completion
test -e $compl && source $compl

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

# If set, the pattern "**" used in a pathname expansion context will
# match all files and zero or more directories and subdirectories.
#shopt -s globstar


if test "$DISPLAY"; then
    # check the window size after each command and, if necessary,
    # update the values of LINES and COLUMNS.
    shopt -s checkwinsize
fi

# launch powerline-rs
function _powerline_rs_prompt() {
    # Switch back to cwd on new terminal opening :
    local pwd='~'
    [ "$PWD" != "$HOME" ] && pwd=${PWD/#$HOME\//\~\/}
    pwd="${pwd//[[:cntrl:]]}"
    printf "\033]0;%s@%s:%s\033\\" "${USER}" "${HOSTNAME%%.*}" "${pwd}"
    printf "\033]7;file://%s%s\033\\" "${HOSTNAME}" "$(/usr/lib/vte-urlencode-cwd)"

    # Set PS1 :
    #PS1="$(powerline-rs --shell bash --theme ~/.config/powerline-rs/adwaita.theme --modules host,ssh,cwd,perms,git,gitstage,nix-shell,root $?)"
    PS1="$(powerline-rs --cwd-max-depth 2 --theme ~/.config/powerline-rs/tweaks.theme --modules ssh,host,cwd,perms,git,gitstage,nix-shell,root $?)"
}
if test $(which powerline-rs 2> /dev/null); then
    PROMPT_COMMAND=_powerline_rs_prompt
else
    # source git prompt script for inclusion in PS1 - now using powerline instead
    source /usr/share/git/completion/git-prompt.sh
    PS1='\[${ORANGE}\]\u@\h:\[${RESET}\] \W\[$GREEN\]$(__git_ps1 " (%s)") \[${ORANGE}\]$ \[${RESET}\]'
    #PS1='\[${ORANGE}\]\u@\h:\[${RESET}\] \W\[$GREEN\]\[${ORANGE}\] $ \[${RESET}\]'

fi

# Display ":l" on the right of the last line if last command failed - see PS1
fancystatus()
{
    if [[ $? != 0 ]]; then
        tput sc; tput cuu 1; tput cuf $((COLUMNS - 2)); echo ":l"; tput rc
        #tput sc; printf "%*s" $COLUMNS ":l"; tput rc
    fi
}
#PROMPT_COMMAND=${PROMPT_COMMAND:+$PROMPT_COMMAND; }'fancystatus'

