# Main shell options - to be sourced by any POSIX shell.
#

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
umask 027

export EDITOR=vim

PATH="$HOME/.gem/ruby/2.7.0/bin:$PATH"
PATH="$HOME/.node_modules/bin:$PATH"

# set PATH so it includes user's private bin if it exists
if test -d "$HOME/.local/bin"; then
    PATH="$HOME/.local/bin:$PATH"
fi

# External programs tweaks
#

# GNU less options
# E --quit-at-eof
# F --quit-if-one-screen
# M --long-prompt : verbose log line at bottom
# q --quiet : no bells/silent
# R --raw-control-chars : turn color escape sequences on
# S --chop-long-lines : disables line wrap
# i --ignore-case : case-insensitive search
export LESS=-FMRi
#export LESS="--quit-if-one-screen --raw-control-chars --ignore-case"
export PAGER="less $LESS"
export BAT_PAGER="less $LESS"

# set systemd's output
# K --quit-on-intra : quit on Ctrl-C
# X --no-init : no termcap init/deinit strings
SYSTEMD_LESS=FKMRX

# make less more friendly for non-text input files, see lesspipe(1)
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
# Stop ruby from printing too much warnings - useful with jekyll
export RUBYOPT=-W0
# Necessary npm config 
export npm_config_prefix=~/.node_modules

# Useful shortcut to estimate division of floats.
calc(){ awk "BEGIN { print "$*" }"; }

# Launch an ssh agent if none is running
#if ! pgrep -u "$USER" ssh-agent > /dev/null; then
#    ssh-agent > ~/.ssh-agent-thing
#fi
#if [[ "$SSH_AGENT_PID" == "" ]]; then
#    eval "$(< ~/.ssh-agent-thing)" > /dev/null
#fi
## shut the agent down on logout - lets ecryptfs neatly autoumount
#trap "pkill ssh-agent && rm ~/.ssh-agent-thing" EXIT


# Color support
# We will try to use the 8 basic colors as semantic marking.

# Do not call dircolors, as ls uses basic colors on its own.
#if test -x /usr/bin/dircolors; then
#  eval "$(dircolors -b)"
#fi

if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
    # We have color support, set a pair of useful colors :
    RED="$(tput setaf 1)"
    GREEN="$(tput setaf 2)"
    ORANGE="$(tput setaf 3)"
    BLUE="$(tput setaf 4)"
    PINK="$(tput setaf 5)"
    CYAN="$(tput setaf 6)"
    YELLOW="$(tput setaf 11)"
    RESET="$(tput sgr0)"
fi

# Base16 Shell color scheme
#BASE16_SHELL="$HOME/.config/base16/shell/"
#[ -n "$PS1" ] && \
#    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
#        eval "$("$BASE16_SHELL/profile_helper.sh")"

# Set up colors defined in .Xresources
#if [ "$TERM" = "linux" ]; then
#    _SEDCMD='s/.*\*color\([0-9]\{1,\}\).*#\([0-9a-fA-F]\{6\}\).*/\1 \2/p'
#    for i in $(sed -n "$_SEDCMD" $HOME/.Xresources | awk '$1 < 16 {printf "\\e]P%X%s", $1, $2}'); do
#        echo -en "$i"
#    done
#    clear
#fi

# Colored GCC warnings and errors. Uses the color codes as described in
# dircolors --print-database
export GCC_COLORS='error=01;31:warning=01;35:note=01;36:caret=01;32:locus=01:quote=01'

# Colors in man - same source for color codes.
man() {
    LESS_TERMCAP_md=$'\e[01;31m' \
    LESS_TERMCAP_me=$'\e[0m' \
    LESS_TERMCAP_se=$'\e[0m' \
    LESS_TERMCAP_so=$'\e[01;44;33m' \
    LESS_TERMCAP_ue=$'\e[0m' \
    LESS_TERMCAP_us=$'\e[01;32m' \
    command man "$@"
}


# Styling
#

# If this is an X terminal set the title to user@host: dir - vte.sh does this,
# check it out below.
#case "$TERM" in
#xterm*|rxvt*)
#    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
#    ;;
#esac

# Deal with vte initialization - Arch doesn't source this on login :
# It's mostly used to switch back to cwd on opening a new terminal window.
# Right now got it working into powerline-rs prompt, see shell-specific rcfile.
#if [ $TILIX_ID ] || [ $VTE_VERSION ]; then
#        source /etc/profile.d/vte.sh
#fi

