# This is Naia prompt, my hand-written prompt for zsh, derived from Maia prompt.

# Enable substitution for prompt :
setopt prompt_subst

# Maia prompt revised.
PROMPT="%B%m %F{cyan}%(4~|%-1~/.../%2~|%~)%u%f%b >%F{cyan}>%B%(?.%F{cyan}.%F{red})>%f%b "

# Modify the colors and symbols in these variables as desired.
GIT_PROMPT_PREFIX="%F{green}[%f"
GIT_PROMPT_SUFFIX="%F{green}]%f"
# plus/minus     - clean repo
GIT_PROMPT_SYMBOL="%F{blue}%1{±%}"
# ⬆"NUM"         - ahead by "NUM" commits
GIT_PROMPT_AHEAD="%F{red}%1{⬆%}NUM%f"
# ⬇"NUM"         - behind by "NUM" commits
GIT_PROMPT_BEHIND="%F{cyan}%1{⬇%}NUM%f"
# lightning bolt - merge conflict
GIT_PROMPT_MERGING="%F{magenta}%1{⚡︎%}%f"
# + sign         - untracked files
GIT_PROMPT_UNTRACKED="%F{red}+%f"
# ✎              - tracked files modified
GIT_PROMPT_MODIFIED="%F{yellow}%2{✎ %}%f"
# ✔              - staged changes present
GIT_PROMPT_STAGED="%F{green}%1{✔%}%f"

parse_git_branch() {
  # Show Git branch/tag, or name-rev if on detached head :
  ( git symbolic-ref -q HEAD || git name-rev --name-only --no-undefined --always HEAD ) 2> /dev/null
}

parse_git_state() {
  # Show different symbols as appropriate for various Git
  # repository states. Compose this value via multiple
  # conditional appends.
  local GIT_STATE=""
  local NUM_AHEAD="$(git log --oneline @{u}.. 2> /dev/null | wc -l | tr -d ' ')"
  if [ "$NUM_AHEAD" -gt 0 ]; then
    GIT_STATE=$GIT_STATE${GIT_PROMPT_AHEAD//NUM/$NUM_AHEAD}
  fi
  local NUM_BEHIND="$(git log --oneline ..@{u} 2> /dev/null | wc -l | tr -d ' ')"
  if [ "$NUM_BEHIND" -gt 0 ]; then
    GIT_STATE=$GIT_STATE${GIT_PROMPT_BEHIND//NUM/$NUM_BEHIND}
  fi
  local GIT_DIR="$(git rev-parse --git-dir 2> /dev/null)"
  if [ -n $GIT_DIR ] && test -r $GIT_DIR/MERGE_HEAD; then
    GIT_STATE=$GIT_STATE$GIT_PROMPT_MERGING
  fi
  if [[ -n $(git ls-files --other --exclude-standard 2> /dev/null) ]]; then
    GIT_STATE=$GIT_STATE$GIT_PROMPT_UNTRACKED
  fi
  if ! git diff --quiet 2> /dev/null; then
    GIT_STATE=$GIT_STATE$GIT_PROMPT_MODIFIED
  fi
  if ! git diff --cached --quiet 2> /dev/null; then
    GIT_STATE=$GIT_STATE$GIT_PROMPT_STAGED
  fi
  if [[ -n $GIT_STATE ]]; then
    echo "$GIT_PROMPT_PREFIX$GIT_STATE$GIT_PROMPT_SUFFIX"
  fi
}

# Async solution found at https://unix.stackexchange.com/a/645949 :
async_git_prompt_callback() {
    RPROMPT="$(<&$1)"
    zle reset-prompt
    zle -F $1
}

async_git_prompt() {
  # If inside a Git repository, print its branch and state :
  if git rev-parse 2> /dev/null; then {
    exec {FD}< <(
      local git_where="$(parse_git_branch)"
      echo -n "$GIT_PROMPT_SYMBOL$(parse_git_state)"
      echo -n "$GIT_PROMPT_PREFIX"
      echo -n "%B%F{cyan}${git_where#(refs/heads/|tags/)}%b"
      echo -n "$GIT_PROMPT_SUFFIX"
    )

    zle -F $FD async_git_prompt_callback
  } else {
    # Clean the RPROMPT when we leave a git repository :
    RPROMPT=""
  } fi
}

precmd_functions+=(async_git_prompt)

