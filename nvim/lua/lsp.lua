local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

-- buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
local opts = { noremap=true, silent=true }
-- buf_set_keymap('n', '<leader>l', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
-- buf_set_keymap('n', '<leader>k', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
-- buf_set_keymap('n', '<leader>j', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)

local lsp_status = require('lsp-status')
lsp_status.register_progress()

require 'lspconfig'.clangd.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
}
require 'lspconfig'.bashls.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
}
require 'lspconfig'.cssls.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
}
require 'lspconfig'.html.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
}
require 'lspconfig'.sumneko_lua.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
  cmd = {'/usr/bin/lua-language-server', '-E', '/usr/share/lua-language-server'}
}
require 'lspconfig'.pyright.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
}
--require 'lspconfig'.rustc.setup {
--  on_attach = lsp_status.on_attach,
--  capabilities = lsp_status.capabilities,
--}
require 'lspconfig'.rls.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
  cmd = {"rustup", "run", "nightly", "rls"},
  settings = {
    rust = {
      unstable_features = true,
      build_on_save = false,
      all_features = true,
    },
  },
}
require 'lspconfig'.tsserver.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
}
require 'lspconfig'.vuels.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
}
require 'lspconfig'.vimls.setup {
  on_attach = lsp_status.on_attach,
  capabilities = lsp_status.capabilities,
}
