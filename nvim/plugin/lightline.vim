scriptencoding utf-8
" Hide mode from last line :
set noshowmode
let g:lightline = {
  \   'colorscheme': 'ThemerVimLightline',
  \   'active': {
  \     'left': [ [ 'mode', 'paste' ], [ 'gitbranch', 'file', 'readonly' ], ],
  \     'right': [ [ 'lspstatus', 'percent', 'lineinfo', 'filetype' ], ],
  \   },
  \   'inactive': {
  \   'left': [ [ 'gitbranch', 'file' ] ],
  \   'right': [],
  \   },
  \   'component': {
  \     'lineinfo': '%l:%v',
  \   },
  \   'component_function': {
  \     'file': 'LightlineFile',
  \     'gitbranch': 'LightlineGitBranch',
  \     'percent': 'LightlinePercent',
  \     'filetype': 'LightlineFileType',
  \   }
  \ }
let g:lightline.separator = {
  \   'left': '', 'right': ''
  \}
let g:lightline.subseparator = {
  \   'left': '', 'right': '' 
  \}
let g:lightline.component_expand = {
  \  'lspstatus': 'LspStatus',
  \ }
let g:lightline.component_type = {
  \     'lspstatus': 'right',
  \ }

" Statusline
function! LspStatus() abort
  if luaeval('#vim.lsp.buf_get_clients() > 0')
    return luaeval("require('lsp-status').status()")
  endif
  return ''
endfunction

function! LightlinePercent()
  return winwidth(0) > 70 ? line('.') * 100 / line('$') . '%' : ''
endfunction

function! LightlineGitBranch()
  return winwidth(0) > 70 ? fugitive#head() : fugitive#head()[0:2]
endfunction

function! LightlineFile()
  " If this is a git object, append [git] to the filename :
  " We could give the branch and revision of this file, see that later.
  if match(expand('%'), '^fugitive') != -1
    let filename = expand('%:t') . ' [git]'
  else
    let filename = expand('%:t')
  endif
  let filename .= &modified ? ' +' : ''
  return filename
endfunction

function! LightlineFileType()
  return winwidth(0) > 70 ? (&filetype !=# '' ? &filetype : 'no ft') : ''
endfunction

function! LightlineFileEncoding()
  return winwidth(0) > 70 ? &fileencoding : ''
endfunction
