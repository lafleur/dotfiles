" Source some defaults
"
"set runtimepath^=~/.vim runtimepath+=~/.vim/after
"let &packpath = &runtimepath
"set runtimepath+=/usr/share/vim/vimfiles
"source ~/.vimrc
source /usr/share/vim/vimfiles/archlinux.vim
runtime defaults.vim


" Behaviour tweaks
"

set mouse=a
" Restore the last cursor position - see :help last-position-jump
autocmd BufReadPost *
  \ if line("'\"") >= 1 && line("'\"") <= line("$") && &ft !~# 'commit'
  \ |   exe "normal! g`\""
  \ | endif
" try to get system clipboard :
"set clipboard+=unnamed
set clipboard+=unnamedplus

" Split on the right side :
set splitright

" Include matching uppercase words with lowercase search term
set ignorecase
" Include only uppercase words with uppercase search term
set smartcase
" Traverse line breaks with arrow keys
"set whichwrap=b,s,<,>,[,]

" Deal with the Esc press timeout; also impacts ??
"set timeoutlen=100

if has('vms')
  set nobackup
else
  "set backup       " keep a backup file (restore to previous version).
  if has('persistent_undo')
    set undofile    " keep an undo file (undo changes after closing).
  endif
endif

" have spell check - can be tiring if you parse code
"set spell

" Save position of the whole vim window (?) :
set sessionoptions^=winpos


" Indentation
"
" Always set autoindenting on :
set autoindent
filetype plugin indent on
" show existing tab with this many spaces width :
set tabstop=2
" when indenting with '>', use this number of spaces width :
set shiftwidth=2
" On pressing tab, insert this same number of spaces :
set expandtab


" Command tweaks
"
let mapleader = ' '

" Move text and rehighlight -- vim tip_id=224 :
vnoremap > ><CR>gv
vnoremap < <<CR>gv
" Let <Shift-u> redo because I never remember ctrl-r mapping.
nmap U <C-r>
" Let <Shift-t> jump to tag because ctr-alt-) is so bad on french keyboards.
nmap T <C-]>
" Esc key twice will exit insert mode in terminal :
tmap <Esc><Esc> <C-\><C-n>

" Change local dir to that of the local file. See :help filename-modifiers .
nmap <leader>cd :lcd %:p:h<CR>


" Presentation
"
colo ThemerVim
" Break lines at word (requires Wrap lines) :
set linebreak
" Wrap-broken line prefix - !!! followed by a '\ ' intentionally !
set showbreak=+++\ 
hi Search cterm=NONE ctermfg=black ctermbg=blue

" Add ligatures - this should appear as 1 character : =>
set guifont=FiraCode\ Nerd\ Font:h14
" Using a fallback - test with :set guifont=...
"set guifont=FiraCode\ Nerd\ Font,TerminessTTF\ Nerd\ Font:h19
"set guifont=FiraCode\ Nerd\ Font,Fira\ Mono\ for\ Powerline:h19


" Neovide specific config
"
" neovide doesn't load ginit.vim, but others guis go there.
if exists('g:neovide')
    "guifont size syntax is not the same in neovide as in other GUIs.
    set guifont=FiraCode\ Nerd\ Font:h18
    "colo base16-adwaita
    "set guioptions+=a
    "vmap <leader>y "+y<CR>
    "vmap <leader>d "+c<CR>
    "vmap <leader>p c<ESC>"+p<CR>

    " Workaround SDL2 AltGr keys not properly read :
    " See https://github.com/Kethku/neovide/issues/151#issuecomment-682123519
    imap <M-~> ~
    imap <M-#> #
    imap <M-{> {
    imap <M-[> [
    imap <M-Bar> \|
    imap <M-`> `
    imap <M-\> \
    imap <M-^> ^
    imap <M-@> @
    imap <M-¤> ¤
    imap <M-}> }
    imap <M-]> ]
    " for command mode:
    "map! <M-C-\> \
    "map! <M-C-[> [
    "map! <M-C-]> ]
    "map! <M-C-`> `
endif

" Load lsp config from ./lua/lsp.lua :
lua require 'lsp'
"set omnifunc=v:lua.vim.lsp.omnifunc
set omnifunc=lsp#complete
nmap <leader>l :LspHover<CR>
nmap <leader>k :LspPreviousDiagnostic<CR>
nmap <leader>j :LspNextDiagnostic<CR>

