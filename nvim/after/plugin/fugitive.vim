" Fugitive plugin shorcuts
"
" Bring the git left menu and make it half wider.
"nmap <leader>fs :call PushToLeft('G')<CR>
nmap <leader>fs :vert lefta G<CR>
" Stage file.
nmap <leader>fa :Gadd %<CR>
" Commit changes.
nmap <leader>fc :vert Gcommit<CR>

nmap <leader>fd :Gvdiffsplit 
" Merging options. The preferred way is DiffConflicts.
nmap <leader>fm :DiffConflicts<CR>
" Option 2 : call vim-mergetool.
nmap <leader>ft <plug>(MergetoolToggle)

" A fugitive git graph
"command -nargs=* Glg Git! lg <args>

" Preferred option : my own git graph :
nmap <leader>fg :Git graph<CR>

" Option 0 : start a git merge conflict resolution as a 3 pane diff.
"nmap <leader>gd :Gvdiffsplit!<CR>
" Merge from left pane in Gdiffsplit. Note that you can 1do in vimdiff.
"nmap gdh :diffget //2<CR> :diffupdate<CR>
" Merge from right pane.
"nmap gdl :diffget //3<CR> :diffupdate<CR>

